package com.liuxd.blog.spring.cloud.alibaba.service1.api;

/**
 * @author liuxd
 * @date 2019/12/14
 * @description
 */
public interface Service1Api {

    /**
     * 获取value
     *
     * @return
     */
    String getValue();

    /**
     * 级联获取service2的value
     *
     * @return
     */
    String getValueWithService2();

}
