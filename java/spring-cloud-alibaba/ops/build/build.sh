#!/bin/bash

service_name=$1
tag=$2

jar_path="/data/jars/"$service_name".jar"
echo '移动jar包:'$jar_path

cp $jar_path app.jar
image_name=$service_name":"$tag
echo "编译镜像 "$image_name
sudo docker build -t $image_name .

sudo docker tag $image_name  registry-server:30005/$image_name
echo "推送镜像"
sudo docker push registry-server:30005/$image_name

echo "部署k8s"

tmp_file_name="tmp-"$service_name".yaml"
cp deployment.yaml $tmp_file_name

echo "替换yaml文件"
sed -i  -e "s@service_name@${service_name}@g" \
          -e  "s@image_name@registry-server:30005/${image_name}@g"  \
          $tmp_file_name

echo "部署新的pods"
kubectl --kubeconfig=/home/liuxiaodong/.kube/config apply -f $tmp_file_name

echo "删除yaml文件"
rm $tmp_file_name