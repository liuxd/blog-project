package com.liuxd.blog.spring.cloud.alibaba.service1.endpoint.controller;

import com.liuxd.blog.spring.cloud.alibaba.service1.api.Service1Api;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liuxd
 * @date 2019/12/15
 * @description
 */
@RestController
public class Service1Controller {

    @Reference(version = "${service1-app.service.version}", timeout = 3000)
    private Service1Api service1Api;

    @Value("${endpoint1.value}")
    private String endpointValue;

    @GetMapping("getValue")
    public String getValue() {
        return service1Api.getValue() + " " + endpointValue;
    }

    @GetMapping("getValueWithService2")
    public String getValueWithService2() {
        return service1Api.getValueWithService2() + " " + endpointValue;
    }
}
