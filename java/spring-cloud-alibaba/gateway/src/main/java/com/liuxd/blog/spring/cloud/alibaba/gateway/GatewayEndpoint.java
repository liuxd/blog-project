package com.liuxd.blog.spring.cloud.alibaba.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.PropertySource;

/**
 * @author liuxd
 * @date 2019/12/14
 * @description
 */
@SpringBootApplication(scanBasePackages = "com.liuxd.blog")
@EnableDiscoveryClient
@PropertySource("classpath:config.properties")
public class GatewayEndpoint {

    public static void main(String[] args) {
        SpringApplication.run(GatewayEndpoint.class, args);
    }

}