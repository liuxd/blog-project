package com.liuxd.blog.spring.cloud.alibaba.service2.api;

/**
 * @author liuxd
 * @date 2019/12/14
 * @description
 */
public interface Service2Api {

    /**
     * 获取value
     *
     * @return
     */
    String getValue();

}
