package com.liuxd.blog.spring.cloud.alibaba.service2.app.service;

import com.liuxd.blog.spring.cloud.alibaba.service2.api.Service2Api;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.io.Serializable;

/**
 * @author liuxd
 * @date 2019-10-23
 */
@Data
@Slf4j
@Service(protocol = "dubbo", version = "${service2-app.service.version}", timeout = 3000)
@RefreshScope
public class Service2Impl implements Service2Api, Serializable {

    @Value("${service2.value}")
    private String value;

    @Override
    public String getValue() {
        return value;
    }

}