package com.liuxd.blog.spring.cloud.alibaba.service2.endpoint;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;

/**
 * @author liuxd
 * @date 2019/12/14
 * @description
 */
@SpringBootApplication(scanBasePackages = "com.liuxd.blog")
@EnableDiscoveryClient
@PropertySource("classpath:config.properties")
public class Service2Endpoint {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(Service2Endpoint.class, args);
	}
}
