package com.liuxd.blog.spring.cloud.alibaba.service2.endpoint.controller;

import com.liuxd.blog.spring.cloud.alibaba.service2.api.Service2Api;
import lombok.Data;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

/**
 * @author liuxd
 * @date 2019/12/15
 * @description
 */
@Data
@RestController
public class Service2Controller implements Serializable {

    @Reference(version = "${service2-app.service.version}", timeout = 3000)
    private Service2Api service2Api;

    @GetMapping("getValue")
    public String getValue(){
        return service2Api.getValue();
    }
}
