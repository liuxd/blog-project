package com.liuxd.blog.spring.cloud.alibaba.service1.app.service;

import com.liuxd.blog.spring.cloud.alibaba.service1.api.Service1Api;
import com.liuxd.blog.spring.cloud.alibaba.service2.api.Service2Api;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.io.Serializable;

/**
 * @author liuxd
 * @date 2019/12/14
 * @description
 */
@Data
@Slf4j
@Service(protocol = "dubbo", version = "${service1-app.service.version}", timeout = 3000)
@RefreshScope
public class Service1Impl implements Service1Api, Serializable {

    @Value("${service1.value}")
    private String value;

    @Reference(version = "${service2-app.service.version}", timeout = 3000)
    private Service2Api service2Api;

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String getValueWithService2() {
        return value + " : " + service2Api.getValue();
    }
}