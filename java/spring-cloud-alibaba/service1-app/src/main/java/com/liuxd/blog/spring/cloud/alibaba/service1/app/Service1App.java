package com.liuxd.blog.spring.cloud.alibaba.service1.app;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.PropertySource;

/**
 * @author liuxd
 * @date 2019/12/14
 * @description
 */
@SpringBootApplication(scanBasePackages = "com.liuxd.blog")
@EnableDiscoveryClient
@PropertySource({"classpath:config.properties"})
public class Service1App {

    public static void main(String[] args) {
        new SpringApplicationBuilder(Service1App.class)
                .web(WebApplicationType.NONE)
                .run(args);
    }
}
